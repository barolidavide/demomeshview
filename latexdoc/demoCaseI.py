import dolfin
from dolfin import *

mesh = UnitSquareMesh(10,10)
# Define SubDomain for right, left and interface domain
right_domain = CompiledSubDomain("x[0] >= 0.5")

interface_domain = CompiledSubDomain("x[0] == 0.5")

left_domain = CompiledSubDomain("x[0] <= 0.5")
# Define the Cell, Edge Function for marking the mesh entities.
cell_func = CellFunction("size_t", mesh,0)
facet_func = EdgeFunction("size_t", mesh,0)
# Mark the cell/facet according with the SubDomain
left_domain.mark(cell_func,1)

right_domain.mark(cell_func,2)

interface_domain.mark(facet_func,3)

# Define the SubManifold using the static method of MeshViewMapping

submanifold_left = MeshViewMapping.create_from_marker(cell_func, 1)

submanifold_right = MeshViewMapping.create_from_marker(cell_func, 2)

submanifold_interface = MeshViewMapping.create_from_marker(facet_func, 3)

element_triangle = FiniteElement("Lagrange",triangle,1)

#Define the Function Space 
V1 = FunctionSpace(submanifold_left,element_triangle)
V2 = FunctionSpace(submanifold_right,element_triangle)

element_line = FiniteElement("Lagrange",interval,1)

VGamma = FunctionSpace(submanifold_interface,element_line)

V = FunctionSpaceProduct(V1,V2,VGamma)

#Define Boundary Condition.
bc_value = Constant(0.0)
left_side=CompiledSubDomain("x[0]==0")
right_side=CompiledSubDomain("x[0]==1.0")

bc1=DirichletBC(V.sub_space(0),bc_value,left_side)
bc2=DirichletBC(V.sub_space(1),bc_value,right_side)








